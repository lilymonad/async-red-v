use core::{
    alloc::{GlobalAlloc, Layout},
    ptr::null_mut,
    mem::size_of,
};

extern "C" {
    static mut _heap_start : ChunkDescriptor;
}

#[repr(C)]
struct ChunkDescriptor {
    data: usize,
}

impl ChunkDescriptor {

    const SIZE_BITS : usize = 0xffff_fffe;
    const TAKEN_BIT : usize = 0x0000_0001;

    #[inline]
    const fn empty() -> Self {
        Self { data: 0 }
    }

    #[inline]
    fn free(&mut self) {
        self.data &= Self::SIZE_BITS;
    }

    #[inline]
    fn take(&mut self) {
        self.data |= Self::TAKEN_BIT;
    }

    #[inline]
    fn is_taken(&self) -> bool {
        self.data & Self::TAKEN_BIT != 0
    }

    #[inline]
    fn set_size(&mut self, size: usize) {
        self.data = size & Self::SIZE_BITS;
    }

    #[inline]
    fn is_empty(&self) -> bool {
        self.size() == 0
    }

    #[inline]
    fn size(&self) -> usize {
        self.data & Self::SIZE_BITS
    }

    #[inline]
    unsafe fn next_mut(&mut self) -> Option<&mut Self> {
        if self.is_empty() {
            None
        } else {
            let self_u8ptr = self as *mut _ as *mut u8;
            let next_u8ptr = self_u8ptr.add(self.size() + size_of::<Self>());
            (next_u8ptr as *mut Self).as_mut()
        }
    }

    #[inline]
    fn u8_addr(&self) -> *mut u8 {
        self as *const _ as *const u8 as *mut u8
    }
}

/// This allocator is veeeery simple. It's a linked list allocator without any fancy fusion
/// mechanism. What is allocated will stay allocated. Freeing only sets the memory location
/// available for another piece of data of same size or less.
struct Allocator {}

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        // compute a layout which represent a structure with the cursor at offset 0
        // followed by the object we want to allocate
        //
        // offset represents the offset from the beginning of this agregate to the start of the
        // user-allocated memory block
        //
        // If it fails then something went wrong in the user side, like a negative size or
        // something like that. So we return a null pointer.
        let (layout, offset) = match Layout::new::<ChunkDescriptor>().extend(layout) {
            Ok((l, o)) => { (l.pad_to_align(), o) },
            _ => { return null_mut() }
        };

        // the total size of the requested memory block + potential padding bytes before it
        let total_size = layout.size() - size_of::<ChunkDescriptor>();
        // a cursor to iterate over allocated chunks
        let mut cursor = Some(&mut _heap_start);
        while let Some(chunk) = cursor {
            // if we are at the end of the list, we allocate a new chunk
            if chunk.is_empty() {
                // set size of last chunk (previously 0 sized)
                chunk.set_size(total_size);
                chunk.take();

                // now that chunk size is set, we can compute next chunk address and then
                // set this new one empty so that we have a new end
                let next = chunk.next_mut();
                *(next.unwrap()) = ChunkDescriptor::empty();

                return chunk.u8_addr().add(offset)
            }

            let size = chunk.size();
            let taken = chunk.is_taken();

            // if the block is available and big enough, we take it
            if size >= total_size && !taken {
                chunk.take();
                return chunk.u8_addr().add(offset);
            }

            cursor = chunk.next_mut();
        }

        null_mut()
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        // by computing the extended layout like in alloc() function, we can retrieve the offset
        // and then get the chunk info position in O(1)
        let (_, offset) = match Layout::new::<ChunkDescriptor>().extend(layout) {
            Ok((l, o)) => { (l.pad_to_align(), o) },
            _ => { return }
        };

        // compute chunk position
        let chunk = (ptr as usize - offset) as *mut ChunkDescriptor;
        (*chunk).free();
    }
}

#[global_allocator]
static ALLOCATOR : Allocator = Allocator {};

pub fn init() {
    unsafe {
        _heap_start = ChunkDescriptor::empty(); // heap starts empty (no chunk allocated)
    }
}

#[alloc_error_handler]
fn alloc_error(layout: Layout) -> ! {
    println!("Program ended because we tried to allocate {:?}", layout);
    loop {}
}

pub fn print_stats() {
    println!("Dynamic memory stats:");
    println!("Chunk list:");
    unsafe {
        let mut cursor = Some(&mut _heap_start);
        let mut total_dynamic_memory = 0;
        while let Some(chunk) = cursor {
            let addr = chunk.u8_addr();
            let size = chunk.size() + size_of::<ChunkDescriptor>();
            println!("\t[0x{:x}, 0x{:x}]; free: {}", addr as usize, addr.add(size) as usize, !chunk.is_taken()); 
            total_dynamic_memory += size;
            cursor = chunk.next_mut();
        }
        println!("Total RAM usage = {} bytes", total_dynamic_memory);
    }
}
