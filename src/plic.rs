const PLIC_SRC_PRIO_BASE : *mut u32 = 0x0C00_0000 as *mut _;
const PLIC_PENDING_BASE : *mut u32 = 0x0C00_1000 as *mut _;
const PLIC_ENABLE_BASE : *mut u32 = 0x0C00_2000 as *mut _;
const PLIC_THRESHOLD : *mut u32 = 0x0C20_0000 as *mut _;
const PLIC_CLAIM_COMPLETE : *mut u32 = 0xC20_0004 as *mut _;

pub fn set_threshold(threshold: u32) {
    unsafe { PLIC_THRESHOLD.write_volatile(threshold & 7) }
}

pub fn claim() -> u32 {
    unsafe { PLIC_CLAIM_COMPLETE.read_volatile() }
}

pub fn complete(intid: u32) {
    unsafe { PLIC_CLAIM_COMPLETE.write_volatile(intid) }
}

pub fn enable_source(intid: u32, prio: u32) {
    unsafe {
        let en_off = (intid / 32) as usize;
        let old = PLIC_ENABLE_BASE.add(en_off).read_volatile();
        PLIC_ENABLE_BASE.add(en_off).write_volatile(old | (1 << (intid % 32)));
        PLIC_SRC_PRIO_BASE.add(intid as usize).write_volatile(prio & 7);
    }
}

pub fn disable_source(intid: u32) {
    unsafe {
        let en_off = (intid / 32) as usize;
        let old = PLIC_ENABLE_BASE.add(en_off).read_volatile();
        PLIC_ENABLE_BASE.add((intid / 32) as usize).write_volatile(old & !(1 << (intid % 32)));
        PLIC_SRC_PRIO_BASE.add(intid as usize).write_volatile(0);
    }
}

pub fn init() {
    unsafe {
        PLIC_ENABLE_BASE.write_volatile(0);
        PLIC_ENABLE_BASE.add(1).write_volatile(0);
        for i in 1..=52 {
            PLIC_SRC_PRIO_BASE.add(i).write_volatile(0);
        }
    }
}
