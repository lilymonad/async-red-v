#[macro_export]
macro_rules! print {
    ($($args:tt)+) => ({
        use core::fmt::Write;
        use crate::uart::Uart0;
        #[allow(unused_unsafe)]
        let _ = write!(Uart0 {}, $($args)+);
    })
}

#[macro_export]
macro_rules! println {
    () => { print!("\r\n") };
    ($fmt:expr) => {
        print!(concat!($fmt, "\r\n"))
    };
    ($fmt:expr, $($args:tt)+) => {
        print!(concat!($fmt, "\r\n"), $($args)+)
    }
}

#[cfg(debug_assertions)]
#[macro_export]
macro_rules! dbg {
    ($($args:tt)+) => ({
        println!($($args)+);
    })
}

#[cfg(not(debug_assertions))]
#[macro_export]
macro_rules! dbg {
    ($($args:tt)+) => ({
    })
}
