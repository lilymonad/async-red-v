use crate::{cpu, time, arv, plic};
use core::{
    arch::asm,
    time::Duration,
};

extern "C" {
    static mut _trap_stack : u8;
}

#[link_section=".vector.trap"]
#[no_mangle]
#[naked]
unsafe extern "C" fn trap_vector() -> ! {
    asm!(
        // get trap frame address, saving t6 value back in mscratch
        "csrrw t6, mscratch, t6",
        // save registers
        "sw  x1,   0(t6)",
        "sw  x2,   4(t6)",
        "sw  x3,   8(t6)",
        "sw  x4,  12(t6)",
        "sw  x5,  16(t6)",
        "sw  x6,  20(t6)",
        "sw  x7,  24(t6)",
        "sw  x8,  28(t6)",
        "sw  x9,  32(t6)",
        "sw x10,  36(t6)",
        "sw x11,  40(t6)",
        "sw x12,  44(t6)",
        "sw x13,  48(t6)",
        "sw x14,  52(t6)",
        "sw x15,  56(t6)",
        "sw x16,  60(t6)",
        "sw x17,  64(t6)",
        "sw x18,  68(t6)",
        "sw x19,  72(t6)",
        "sw x20,  76(t6)",
        "sw x21,  80(t6)",
        "sw x22,  84(t6)",
        "sw x23,  88(t6)",
        "sw x24,  92(t6)",
        "sw x25,  96(t6)",
        "sw x26, 100(t6)",
        "sw x27, 104(t6)",
        "sw x28, 108(t6)",
        "sw x29, 112(t6)",
        "sw x30, 116(t6)",
        // save trap frame address to a0
        "mv a0, t6",
        // restore t6 (which was in mscratch)
        "csrrw t6, mscratch, t6",
        // save t6
        "sw x31, 120(a0)",
        // setup trap_handler parameters (a0 = mcause, a1 = mepc, a2 = mtval)
        "csrr a1, mepc",
        "sw a1, 124(a0)", // also set 'pc' field of trap frame to MEPC
        "csrr a0, mcause",
        "csrr a2, mtval",
        "la sp, {trap_stack}",
        // call handler
        "call {handler}",
        // restore registers
        "csrr t6, mscratch",
        // load mepc
        "lw  a0, 124(t6)",
        "csrw mepc, a0",
        // load the rest of all registers
        "lw  x1,   0(t6)",
        "lw  x2,   4(t6)",
        "lw  x3,   8(t6)",
        "lw  x4,  12(t6)",
        "lw  x5,  16(t6)",
        "lw  x6,  20(t6)",
        "lw  x7,  24(t6)",
        "lw  x8,  28(t6)",
        "lw  x9,  32(t6)",
        "lw x10,  36(t6)",
        "lw x11,  40(t6)",
        "lw x12,  44(t6)",
        "lw x13,  48(t6)",
        "lw x14,  52(t6)",
        "lw x15,  56(t6)",
        "lw x16,  60(t6)",
        "lw x17,  64(t6)",
        "lw x18,  68(t6)",
        "lw x19,  72(t6)",
        "lw x20,  76(t6)",
        "lw x21,  80(t6)",
        "lw x22,  84(t6)",
        "lw x23,  88(t6)",
        "lw x24,  92(t6)",
        "lw x25,  96(t6)",
        "lw x26, 100(t6)",
        "lw x27, 104(t6)",
        "lw x28, 108(t6)",
        "lw x29, 112(t6)",
        "lw x30, 116(t6)",
        "lw x31, 120(t6)",
        // return from interrupt
        "mret",
        trap_stack = sym _trap_stack,
        handler = sym trap_handler,
        options(noreturn)
    )
}

fn trap_handler(mcause: usize, mepc: usize, mtval: usize) {
    let cause = mcause & 0x0fff_ffff;
    let is_interrupt = (mcause & 0x8000_0000) != 0;
    if is_interrupt {
        match cause {
            7 => {
                time::set_timeout(Duration::from_millis(1));
                arv::time::update();
            },
            11 => {
                let id = plic::claim();
                match id {
                    3 => {
                        arv::uart::update();
                    },
                    _ => {},
                }
                plic::complete(id);
            },
            _ => println!("interrupt"),
        }
    } else {
        panic!("Exception: mcause = {:x}, mepc = 0x{:x}, mtval = {:x}", mcause, mepc, mtval);
    }
}

#[repr(C)]
#[derive(Default, Debug)]
pub struct Frame {
    pub ra: usize,
    pub sp: usize,
    pub gp: usize,
    pub tp: usize,
    pub t0: usize,
    pub t1: usize,
    pub t2: usize,
    pub s0: usize,
    pub s1: usize,
    pub a0: usize,
    pub a1: usize,
    pub a2: usize,
    pub a3: usize,
    pub a4: usize,
    pub a5: usize,
    pub a6: usize,
    pub a7: usize,
    pub s2: usize,
    pub s3: usize,
    pub s4: usize,
    pub s5: usize,
    pub s6: usize,
    pub s7: usize,
    pub s8: usize,
    pub s9: usize,
    pub s10: usize,
    pub s11: usize,
    pub t3: usize,
    pub t4: usize,
    pub t5: usize,
    pub t6: usize,
    pub pc: usize,
}

impl Frame {
    const fn zero() -> Frame {
        Frame {
            ra: 0,
            sp: 0,
            gp: 0,
            tp: 0,
            t0: 0,
            t1: 0,
            t2: 0,
            s0: 0,
            s1: 0,
            a0: 0,
            a1: 0,
            a2: 0,
            a3: 0,
            a4: 0,
            a5: 0,
            a6: 0,
            a7: 0,
            s2: 0,
            s3: 0,
            s4: 0,
            s5: 0,
            s6: 0,
            s7: 0,
            s8: 0,
            s9: 0,
            s10: 0,
            s11: 0,
            t3: 0,
            t4: 0,
            t5: 0,
            t6: 0,
            pc: 0,
        }
    }
}

static mut TRAP_FRAME : Frame = Frame::zero();

pub fn init() {
    cpu::mscratch_w(unsafe { &TRAP_FRAME as *const _ as usize });
    cpu::mtvec_w(trap_vector as usize);
}
