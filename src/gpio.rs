const GPIO_BASE : *mut u32 = 0x1001_2000 as _;
const INPUT_VAL_OFF : usize = 0;
const INPUT_EN_OFF : usize = 1;
const OUTPUT_EN_OFF : usize = 2;
const OUTPUT_VAL_OFF : usize = 3;
const IOF_EN_OFF : usize = 14;
const IOF_SEL_OFF : usize = 15;

// disable input mode for a given pin
#[inline]
pub fn disable_input(pin: u32) {
    unsafe {
        let ptr = GPIO_BASE.add(INPUT_EN_OFF);
        let old = ptr.read_volatile();
        ptr.write_volatile(old & !(1 << pin));
    }
}

// enable output for a given pin
#[inline]
pub fn enable_output(pin: u32) {
    unsafe {
        let ptr = GPIO_BASE.add(OUTPUT_EN_OFF);
        let old = ptr.read_volatile();
        ptr.write_volatile(old | (1 << pin));
    }
}

// set the output state of a given pin
#[inline]
pub fn output_set(pin: u32, on: bool) {
    unsafe {
        let ptr = GPIO_BASE.add(OUTPUT_VAL_OFF);
        let old = ptr.read_volatile();

        if on {
            ptr.write_volatile(old | (1 << pin));
        } else {
            ptr.write_volatile(old & !(1 << pin));
        }
    }
}

// disable IOF mode for a given pin
#[inline]
pub fn disable_iof(pin: u32) {
    unsafe {
        let ptr = GPIO_BASE.add(IOF_EN_OFF);
        let old = ptr.read_volatile();
        ptr.write_volatile(old & !(1 << pin));
    }
}

// enable IOF mode for a given pin
#[inline]
pub fn enable_iof(pin: u32) {
    unsafe {
        let ptr = GPIO_BASE.add(IOF_EN_OFF);
        let old = ptr.read_volatile();
        ptr.write_volatile(old | (1 << pin));
    }
}

// These are the two modes for IOF GPIO, refer to the FE310 manual for mode information about that
#[repr(u32)]
pub enum Iof {
    One = 0,
    Two = 1,
}

// select IOF mode for a given pin
#[inline]
pub fn select_iof(pin: u32, which_one: Iof) {
    unsafe {
        let ptr = GPIO_BASE.add(IOF_SEL_OFF);
        let old = ptr.read_volatile();
        match which_one {
            Iof::One => ptr.write_volatile(old & !(1 << pin)),
            Iof::Two => ptr.write_volatile(old | (1 << pin)),
        }
    }
}
