use crate::gpio::{self, Iof};
use core::fmt::{Write, Result};

const UART_BASE : *mut u32 = 0x1001_3000 as _;
const TX_DATA_OFF : usize = 0;
const RX_DATA_OFF : usize = 1;
const TX_CTRL_OFF : usize = 2;
const RX_CTRL_OFF : usize = 3;
const IE_OFF : usize = 4;

pub fn init() {
    gpio::enable_iof(16);
    gpio::enable_iof(17);
    gpio::select_iof(16, Iof::One);
    gpio::select_iof(17, Iof::One);

    unsafe {
        let tx_ctrl = UART_BASE.add(TX_CTRL_OFF);
        let rx_ctrl = UART_BASE.add(RX_CTRL_OFF);
        let ie = UART_BASE.add(IE_OFF);

        tx_ctrl.write_volatile(1);
        rx_ctrl.write_volatile(1);
        ie.write_volatile(2);
    }
}

#[inline]
pub fn tx_u8(c: u8) {
    while !try_tx_u8(c) {}
}

#[inline]
pub fn try_tx_u8(c: u8) -> bool {
    unsafe {
        let ptr = UART_BASE.add(TX_DATA_OFF);
        if ptr.read_volatile() & 0x8000_0000 != 0 { return false }
        ptr.write_volatile(c as u32);
        true
    }
}

#[inline]
pub fn rx_u8() -> u8 {
    loop {
        if let Some(c) = try_rx_u8() {
            return c;
        }
    }
}

#[inline]
pub fn try_rx_u8() -> Option<u8> {
    unsafe {
        let ptr = UART_BASE.add(RX_DATA_OFF);
        let v = ptr.read_volatile();
        if (v & 0x8000_0000) != 0 { return None }
        Some(v as u8)
    }
}

pub struct Uart0 {}

impl Write for Uart0 {
    fn write_str(&mut self, s: &str) -> Result {
        s.bytes().for_each(tx_u8);
        Ok(())
    }
}
