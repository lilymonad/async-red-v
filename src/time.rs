use core::time::Duration;

// some constants related to the fe310 g0002
// to sleep, we will use the mtime counter which tracks a 32KHz clock
pub const MTIME_MILLI : u64 = (1 << 15) / 1000;
const MTIME_BASE : *mut u32 = 0x0200_bff8 as _;
const MTIME_CMP_BASE : *mut u32 = 0x0200_4000 as _;

pub fn mtime64_r() -> u64 {
    unsafe {
        let high = MTIME_BASE.add(1).read_volatile();
        let low = MTIME_BASE.read_volatile();
        let high2 = MTIME_BASE.add(1).read_volatile();
        if high != high2 {
            (high2 as u64) << 32 | MTIME_BASE.read_volatile() as u64
        } else {
            (high as u64) << 32 | low as u64
        }
    }
}

pub fn mtimecmp64_w(time: u64) {
    unsafe {
        MTIME_CMP_BASE.write_volatile(0xffff_ffff);
        MTIME_CMP_BASE.add(1).write_volatile((time >> 32) as u32);
        MTIME_CMP_BASE.write_volatile(time as u32);
    }
}

pub fn set_timeout(time: Duration) {
    let millis = time.as_millis() as u64;
    mtimecmp64_w(mtime64_r() + millis * MTIME_MILLI);
}

pub fn enable_interrupt() {
    crate::cpu::mie_s(0x080)
}
