#![no_std]
#![no_main]
#![feature(
    asm_sym,
    naked_functions,
    concat_idents,
    alloc_error_handler,
    panic_info_message,
    async_closure,
    drain_filter
)]

extern crate alloc;
use core::{
    cell::Cell,
    arch::asm,
    panic::PanicInfo,
    ptr,
    time::Duration,
};

mod gpio;
mod uart;
#[macro_use]
mod print;
mod mem;
mod mpsc;
mod arv;
mod trap;
mod cpu;
mod time;
mod plic;

extern "C" {
    // these symbols are provided by the linker script (layout.ld)
    static mut _data_start : u8;
    static mut _data_end : u8;
    static mut _bss_start : u8;
    static mut _bss_end : u8;
    static mut _got_start : u8;
    static mut _got_end : u8;
    static mut _heap_start : u8;
    static _rwdata : u8;
}

// rust's panic handler. this function is called when a panic!() occurs. it can be for various
// reasons like unwrapping an empty Option, or event arithmetic overflow in debug builds
#[panic_handler]
fn panic(info: &PanicInfo<'_>) -> ! {
    println!("Kernel Panic!");
    if let Some(p) = info.location() {
        println!("\t{}@{}: {}", p.file(), p.line(), info.message().unwrap());
    } else {
        println!("\tNo information available");
    }

    loop {}
}

// every program needs a starting point. we use the conventional '_start' symbol. no_mangle means
// its name in the final elf file will be the exact name of the function. naked means _start is NOT
// a function but instead a big block or raw assembly. By doing this, we avoid writing separate asm
// files and a custom build script.
#[naked]
#[no_mangle]
#[link_section=".start"]
unsafe extern "C" fn _start() -> ! {
    asm!(
        "li sp, 0x80004000", // stack pointer to end of ram TODO: change it to a symbol from layout.ld
        "la gp, {got}",
        "j {main}",
        got = sym _got_start,
        main = sym main,
        options(noreturn)
    );
}

fn print_help() {
    println!();
    println!("COMMAND : DESCRIPTION");
    println!("period  : set blink period of led");
    println!("mem     : print memory stats");
}

// the first Rust function called by the machine
fn main() -> ! {
    // initialize read/write static data
    unsafe {
        let bss_size = &_bss_end as *const _ as usize - &_bss_start as *const _ as usize;
        ptr::write_bytes(&mut _bss_start as _, 0, bss_size);

        let rw_size = &_data_end as *const _ as usize - &_got_start as *const _ as usize;
        ptr::copy_nonoverlapping(&_rwdata as _, &mut _got_start as _, rw_size);
    }

    // now we can use dynamic allocation ! (Box, Vec, String, ...)
    mem::init();

    // now we can println !
    uart::init();

    // setup plic
    plic::init();
    plic::set_threshold(0);
    plic::enable_source(3, 7);

    // init trap vector
    trap::init();

    println!("setup interrupts");
    time::set_timeout(Duration::from_millis(10));
    cpu::mie_w(0x880);
    cpu::mstatus_s(0x8);
    cpu::wfi();

    // setup gpio pin 5 (the blue LED of the RED-V)
    gpio::disable_input(5);
    gpio::disable_iof(5);
    gpio::enable_output(5);
    gpio::output_set(5, true);


    println!("hello there ! go ahead, type something !");

    let period = Cell::new(500);

    let rt = arv::runtime::Executor::new();

    rt.spawn(async { println!("lol"); arv::time::wait(Duration::from_millis(5000)).await; println!("cul") });
    rt.spawn(async {
        let mut on = true;
        loop {
            gpio::output_set(5, on);
            arv::time::wait(Duration::from_millis(period.get())).await;
            on = !on;
        }
    });

    rt.spawn(async {
        loop {
            print!("\r\n> ");
            match arv::uart::read_line().await.as_ref() {
                "period" => {
                    print!("\r\nWhich period you want for the led ?: ");
                    let x = arv::uart::read_line().await.parse();
                    match x {
                        Err(_) => println!("\r\ncannot parse period value"),
                        Ok(x) => period.set(x),
                    }
                },
                "mem" => { println!(); mem::print_stats() },
                "help" => print_help(),
                s => { println!("\r\nunknown command {:?}", s); },
            }
        }
    });

    rt.run();

    loop {}
}
