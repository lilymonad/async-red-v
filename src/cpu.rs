use paste::paste;
use core::arch::asm;

macro_rules! csr {
    {$($name:ident),*} => {
        $(paste! {
            #[inline]
            pub fn [<$name _r>]() -> usize {
                let ret: usize;
                unsafe { asm!(concat!("csrr {0}, ", stringify!($name)), out(reg) ret); }
                ret
            }
            #[inline]
            pub fn [<$name _w>](value: usize) {
                unsafe { asm!(concat!("csrw ", stringify!($name), ", {0}"), in(reg) value); }
            }
            #[inline]
            pub fn [<$name _rw>](mut value: usize) -> usize {
                unsafe { asm!(concat!("csrrw {0}, ", stringify!($name), ", {0}"), inout(reg) value); }
                value
            }
            #[inline]
            pub fn [<$name _s>](value: usize) {
                unsafe { asm!(concat!("csrs ", stringify!($name), ", {0}"), in(reg) value) }
            }
            #[inline]
            pub fn [<$name _rs>](mut value: usize) -> usize {
                unsafe { asm!(concat!("csrrs {0}, ", stringify!($name), ", {0}"), inout(reg) value); }
                value
            }
            #[inline]
            pub fn [<$name _c>](value: usize) {
                unsafe { asm!(concat!("csrc ", stringify!($name), ", {0}"), in(reg) value) }
            }
            #[inline]
            pub fn [<$name _rc>](mut value: usize) -> usize {
                unsafe { asm!(concat!("csrrc {0}, ", stringify!($name), ", {0}"), inout(reg) value); }
                value
            }
        })*
    }
}

csr!{
    mscratch,
    mtvec,
    mie,
    mstatus,
    mepc,
    minstret,
    minstreth,
    mcycle,
    mcycleh
}

#[inline]
pub fn wfi() {
    unsafe { asm!("wfi") }
}

pub fn minstret64_r() -> u64 {
    let low = minstret_r();
    let high = minstreth_r();
    let low2 = minstret_r();
    let high2 = minstreth_r();

    if low2 < low {
        (high2 as u64) << 32 | low2 as u64
    } else {
        (high as u64) << 32 | low as u64
    }
}

pub fn mcycle64_r() -> u64 {
    let low = mcycle_r();
    let high = mcycleh_r();
    let low2 = mcycle_r();
    let high2 = mcycleh_r();

    if low2 < low {
        (high2 as u64) << 32 | low2 as u64
    } else {
        (high as u64) << 32 | low as u64
    }
}
