use core::future::Future;
use crate::{
    mpsc::{channel, Receiver, Sender, ReceiveError},
};
use core::task::{Context, Poll, Waker};
use core::pin::Pin;
use core::marker::PhantomData;
use core::intrinsics::transmute;
use alloc::boxed::Box;

// SAFETY: this vtable is safe because the only place we are managing wakers is in the Executor
// which is single-thread
use core::task::{RawWaker, RawWakerVTable};
static VTABLE : RawWakerVTable = RawWakerVTable::new(
    |ptr| -> RawWaker { // clone
        unsafe {
            let task_ptr = ptr as *mut Task;
            let task_ref = task_ptr.as_mut().unwrap();
            task_ref.ref_count += 1; // we create another waker
            RawWaker::new(ptr, &VTABLE)
        }
    },
    |ptr| { // wake
        unsafe {
            let task_ptr = ptr as *mut Task;
            let task_ref = task_ptr.as_mut().unwrap();
            task_ref.task_sender.send(task_ptr).unwrap(); // no ref decrement because pointer is "moved" (called waker droped)
        }
    },
    |ptr| { // wake by ref
        unsafe {
            let task_ptr = ptr as *mut Task;
            let task_ref = task_ptr.as_mut().unwrap();
            task_ref.ref_count += 1; // ref increment because we woke up by ref and called waker didnt drop
            task_ref.task_sender.send(task_ptr).unwrap();
        }
    },
    |ptr| { // drop
        unsafe {
            let task_ptr = ptr as *mut Task;
            let task_ref = task_ptr.as_mut().unwrap();
            task_ref.ref_count -= 1;

            if task_ref.ref_count == 0 {
                core::ptr::drop_in_place(task_ptr);
            }
        }
    }
);

struct Task {
    future: Option<Pin<Box<dyn Future<Output = ()> + 'static>>>,
    ref_count: usize,
    task_sender: Sender<*mut Self>,
}

pub struct Executor<'rt>
{
    protospawner: Option<Sender<*mut Task>>,
    tasks: Receiver<*mut Task>,
    _marker: PhantomData<fn(& 'rt ()) -> & 'rt ()>,
}

impl<'rt> Executor<'rt>
{
    pub fn new() -> Self {
        let (sender, receiver) = channel(128);
        let rt = Self {
            protospawner: Some(sender),
            tasks: receiver,
            _marker: PhantomData,
        };
        rt
    }

    pub fn run(mut self) {
        drop(self.protospawner.take());
        loop {
            let wait = match self.tasks.recv() {
                Ok(task) => {
                    let tptr = task;

                    // SAFETY: This is safe because we know we never read or write the Task outside
                    // this function.
                    let tref = unsafe { tptr.as_mut().unwrap() };
                    if let Some(mut the_future) = tref.future.take() {

                        // SAFETY: we already have to decrement the reference count because of the
                        // raw pointer we got from the Receiver. We can skip the "increment" part of
                        // the Waker creation, to let the Dropping decrement the raw reference
                        let waker = unsafe { Waker::from_raw(RawWaker::new(tptr as *const (), &VTABLE)) };
                        let mut context = Context::from_waker(&waker);


                        if Poll::Pending == the_future.as_mut().poll(&mut context) {
                            tref.future = Some(the_future)
                        } else {
                        }

                        // Here we drop the created waker which will decrement the ref counter once
                    }

                    false
                },
                Err(ReceiveError::Empty) => {
                    true
                },
                Err(ReceiveError::NoSender) => { break },
            };

            if wait {
                crate::cpu::wfi();
            }
        }
    }

    pub fn spawn<'fut, 'rtref, Fut>(& 'rtref self, f: Fut)
        where Fut : Future<Output = ()> + 'fut,
              'rt: 'rtref,
              'fut: 'rt,
    {
        let task = Box::leak(Box::new(Task {
            future: Some(unsafe {
                let pin : Pin<Box<dyn Future<Output=()> + 'fut>> = Box::pin(f);
                transmute::<_, Pin<Box<dyn Future<Output=()> + 'static>>>(pin)
            }),
            ref_count: 1,
            task_sender: self.protospawner.clone().unwrap(),
        }));

        self.protospawner.as_ref().unwrap().send(task).unwrap();
    }

    pub fn get_a<'a>(& 'a self) -> A<'a>
//        where 'rt: 'a,
    {
        A { _marker: PhantomData }
    }
}

#[derive(Debug)]
pub struct A<'a> {
    _marker: PhantomData<fn(& 'a ()) -> & 'a ()>,
}

