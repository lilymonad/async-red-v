use crate::{print, uart};
use core::{
    task::{Waker, Poll, Context},
    future::Future,
    pin::Pin,
};
use alloc::string::String;

static mut PENDING_UART0: Option<Waker> = None;
static mut UART0_BUFFER: String = String::new();

pub struct UartReadLine {
    buffer: Option<String>,
}

impl Future for UartReadLine {
    type Output = String;
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        unsafe {
            let mut buffer = self.buffer.take().unwrap();
            while UART0_BUFFER.len() > 0 {
                let c = UART0_BUFFER.remove(0);
                match c {
                    '\x08' => { buffer.pop(); },
                    '\r' => { return Poll::Ready(buffer); },
                    c if !c.is_control() => { buffer.push(c); }
                    _ => { }
                }
            }
            self.buffer = Some(buffer);
            PENDING_UART0 = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

pub fn read_line() -> UartReadLine {
    UartReadLine { buffer: Some(String::new()) }
}

pub fn update() {
    while let Some(c) = uart::try_rx_u8() {
        let c = c as char;
        match c {
            '\x08' => {
                print!("\x08 \x08");
            },
            c if c == '\r' || !c.is_control() => {
                print!("{}", c);
            },
            _ => {},
        }
        unsafe {
            UART0_BUFFER.push(c);
            if c == '\r' {
                PENDING_UART0.take().map(|waker| waker.wake());
            }
        }
    }
}
