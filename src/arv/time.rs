use core::{
    task::{Context, Poll, Waker},
    future::Future,
    pin::Pin,
    time::Duration,
};
use alloc::vec::Vec;
use crate::time;

static mut TASKS : Vec<(u64, Waker)> = Vec::new();

pub struct WaitTask {
    to_wait: Option<u64>,
}

impl Future for WaitTask {
    type Output = ();
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(t) = self.to_wait.take() {
            unsafe { TASKS.push((t, cx.waker().clone())) }
            Poll::Pending
        } else {
            Poll::Ready(())
        }
    }
}

pub fn wait(dur: Duration) -> WaitTask {
    let millis = dur.as_millis() as u64;
    let current = time::mtime64_r();
    let to_wait = current + millis * time::MTIME_MILLI;

    WaitTask { to_wait: Some(to_wait) }
}

pub fn update() {
    unsafe {
        let new_time = time::mtime64_r();
        TASKS
            .drain_filter(|(time, _)| *time <= new_time)
            .for_each(|(_, w)| {
                w.wake()
            });
    }
}
