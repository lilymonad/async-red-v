use alloc::{alloc::alloc, alloc::Layout, rc::{Rc, Weak}};
use core::{
    ops::Drop,
    ptr::drop_in_place,
    cell::Cell,
};

struct Fifo<T> {
    size: Cell<usize>,
    read_head: Cell<usize>,
    capacity: usize,
    ptr: *mut T,
}

impl<T:Sized> Fifo<T> {
    fn with_capacity(capacity: usize) -> Self {
        let ret = Self {
            size: Cell::new(0),
            read_head: Cell::new(0),
            capacity,
            ptr: unsafe { alloc(Layout::array::<T>(capacity).unwrap()) as *mut T },
        };
        ret
    }

    fn push(&self, object: T) -> Option<()> {
        if self.size.get() >= self.capacity { return None }
        let index = (self.read_head.get() + self.size.get()) % self.capacity;
        self.size.set(self.size.get() + 1);
        unsafe { self.ptr.add(index).write(object); }
        Some(())
    }

    fn pop(&self) -> Option<T> {
        if self.size.get() == 0 { return None }
        let ret = Some(unsafe { self.ptr.add(self.read_head.get()).read()});
        self.size.set(self.size.get() - 1);
        self.read_head.set((self.read_head.get() + 1) % self.capacity);
        ret
    }
}

impl<T> Drop for Fifo<T> {
    fn drop(&mut self) {
        let mut i = self.read_head.get();
        let mut s = self.size.get();
        while s > 0 {
            unsafe { drop_in_place(self.ptr.add(i)); }
            i = (i + 1) % self.capacity;
            s -= 1;
        }
    }
}

pub struct Sender<T> {
    fifo: Weak<Fifo<T>>,
}

#[derive(Clone, Copy, Debug)]
pub enum SendError {
    Full,
    NoReceiver,
}

impl<T:Sized> Sender<T> {
    pub fn send(&self, object: T) -> Result<(), SendError> {
        let fifo = self.fifo.upgrade().ok_or(SendError::NoReceiver)?;
        fifo.push(object).ok_or(SendError::Full)
    }
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        Self { fifo: self.fifo.clone() }
    }
}

pub struct Receiver<T> {
    fifo: Rc<Fifo<T>>,
}

#[derive(Clone, Copy, Debug)]
pub enum ReceiveError {
    Empty,
    NoSender,
}

impl<T:Sized> Receiver<T> {
    pub fn recv(&self) -> Result<T, ReceiveError> {
        self.fifo.pop().ok_or_else(|| {
            if Rc::weak_count(&self.fifo) == 0 {
                ReceiveError::NoSender
            } else {
                ReceiveError::Empty
            }
        })
    }
}

pub fn channel<T>(capacity: usize) -> (Sender<T>, Receiver<T>) {
    let fifo = Rc::new(Fifo::with_capacity(capacity));
    (Sender { fifo: Rc::downgrade(&fifo) }, Receiver { fifo })
}
