# async-red-v

This is (will be) a proof of concept Rust async runtime for the [SparkFun RED-V RedBoard](https://www.sparkfun.com/products/15594)

If you have one, you can try this program for yourself.

# How to

In order to build and upload the program, you will need Rust (of course), `cargo`, and
`platformio` with the `sifive` platform installed. You will also need a `rv32imac`
compiler.
Build and upload the program with one of these commands (for debug or release versions):
```
cargo run
```
```
cargo run --release
```

If it gives you an error about `firmware.elf` not having any section, re-run the
upload command. I have not the time/energy to define a proper custom platform,
and this is the only way to have our own build script when using platformio.
