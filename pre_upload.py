import os

binary = 'target/riscv32imac-unknown-none-elf/release/async-red-v'
env_binary = os.environ.get('REDV_BIN_PATH')
if env_binary is not None:
    binary = env_binary
pio_dir = '.pio/build/sparkfun_redboard_v'

print(f"Uploading this binary: {binary}")

os.system(f'mkdir -p {pio_dir}')
os.system(f'cp -f {binary} {pio_dir}/firmware.elf')
os.system(f'riscv32-elf-strip {pio_dir}/firmware.elf')
